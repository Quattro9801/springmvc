package com.mycompany.springcourse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author
 */
@Controller
public class HelloController {

    @GetMapping("/hello")
    public String sayHello() {
        return "hello_world";
    }
    @GetMapping("/end")
    public String sayEnd() {
        return "end";
    }
}
